def sumar(x, y=0, z=0):  # ARGUMENTO
    resultado = int(x)+int(y)+int(z)
    return resultado

def restar(x, y=0, z=0):  # ARGUMENTO
    resultado = int(x)-int(y)-int(z)
    return resultado

## PROCEDIMIENTO
def imprimir(a):
    return a

arreglo = {'1': "HOLA", '2': "chao"}
print( imprimir(arreglo) )
variable1 = input("Ingrese primer valor a sumar")
variable2 = input("Ingrese segundo numero a sumar")
s1 = sumar(variable1, variable2)   # PARAMETRO
print("El resultado de la sumar es:", s1)

variable3 = 60
variable4 = 70
variable5 = 100
resul = sumar(variable3, variable4, variable5)
print("El resultado de la sumar es:", resul)
